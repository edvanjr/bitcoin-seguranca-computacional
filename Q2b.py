from sys import exit
from bitcoin.core.script import *

from lib.utils import *
from lib.config import (my_private_key, my_public_key, my_address,
                    faucet_address, network_type)
from Q1 import P2PKH_scriptPubKey
from Q2a import Q2a_txout_scriptPubKey


######################################################################
# Parametros que foram preenchidos
amount_to_send = 0.000009 # amount of BTC in the output you're sending minus fee
txid_to_spend = ('3fbaccab2c0f81bc4844c3b20db0c58048db972b4e6313381ad34fccd516bf8a')
utxo_index = 0 # index of the output you are spending, indices start at 0
######################################################################

txin_scriptPubKey = Q2a_txout_scriptPubKey
######################################################################
# Implementacao que foi alterada
# Implement the scriptSig for redeeming the transaction created
# in  Exercise 2a.
txin_scriptSig = [
        2659, # valores adicionados
        -2080
]
######################################################################
txout_scriptPubKey = P2PKH_scriptPubKey(faucet_address)

response = send_from_custom_transaction(
    amount_to_send, txid_to_spend, utxo_index,
    txin_scriptPubKey, txin_scriptSig, txout_scriptPubKey, network_type)
print(response.status_code, response.reason)
print(response.text)
