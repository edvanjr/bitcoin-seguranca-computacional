from sys import exit
from bitcoin.core.script import *
from bitcoin.wallet import CBitcoinSecret

from lib.utils import *
from lib.config import (my_private_key, my_public_key, my_address,
                    faucet_address, network_type)
from Q1 import send_from_P2PKH_transaction


cust1_private_key = CBitcoinSecret('cPAJ6nJSuXihfyBMJYhKokXnDeaPc2oWUp7BtoAhUtaFdiTQpsys')
cust1_public_key = cust1_private_key.pub
cust2_private_key = CBitcoinSecret('cUu9tTmzoRm1J87HX2cXMnKeYu3YXRUkT5idDWn4LgfJS55WosF1')
cust2_public_key = cust2_private_key.pub
cust3_private_key = CBitcoinSecret('cNvBLPLNPn3DLb9BhnXcsxkCoj3rre8m1TTe3pNLWJQvCipNmkBU')
cust3_public_key = cust3_private_key.pub


######################################################################
# TODO: Complete the scriptPubKey implementation for Exercise 3

# You can assume the role of the bank for the purposes of this problem
# and use my_public_key and my_private_key in lieu of bank_public_key and
# bank_private_key.

Q3a_txout_scriptPubKey = [
    my_public_key,
    OP_CHECKSIGVERIFY,
    1,
    cust1_public_key,
    cust2_public_key,
    cust3_public_key,
    3,
    OP_CHECKMULTISIG
]
######################################################################

if __name__ == '__main__':
    ######################################################################
    # Parametros que foram preenchidos
    amount_to_send = 0.00001 # amount of BTC in the output you're sending minus fee
    txid_to_spend = ('edc6c09e016720cbe28e5e35f8e1a28ce894f1a79ecd3ae18f5a84eb6e497e7a')
    utxo_index = 1 # index of the output you are spending, indices start at 0
    ######################################################################

    response = send_from_P2PKH_transaction(amount_to_send, txid_to_spend, 
        utxo_index, Q3a_txout_scriptPubKey, my_private_key, network_type)
    print(response.status_code, response.reason)
    print(response.text)
