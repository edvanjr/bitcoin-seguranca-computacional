from sys import exit
from bitcoin.core.script import *

from lib.utils import *
from lib.config import (my_private_key, my_public_key, my_address,
                    faucet_address, network_type)
from Q1 import send_from_P2PKH_transaction


######################################################################
# Implementacao que foi alterada
# Complete the scriptPubKey implementation for Exercise 2
Q2a_txout_scriptPubKey = [
    OP_2DUP,
    OP_ADD,
    579,
    OP_EQUALVERIFY,
    OP_SUB,
    4739,
    OP_EQUAL
]
######################################################################

if __name__ == '__main__':
    ######################################################################
    # Parametros que foram preenchidos
    amount_to_send = 0.00001 # amount of BTC in the output you're sending minus fee
    txid_to_spend = ('230a82c8d66ff84553eea373b72b331791b6b00169a51e22a8c6b93f6a3a813a')
    utxo_index = 0 # index of the output you are spending, indices start at 0
    ######################################################################

    response = send_from_P2PKH_transaction(
        amount_to_send, txid_to_spend, utxo_index,
        Q2a_txout_scriptPubKey, my_private_key, network_type)
    print(response.status_code, response.reason)
    print(response.text)
